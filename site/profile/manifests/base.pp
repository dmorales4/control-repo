class profile::base (
  String $user,
) {

  #the base profile should include component modules that will be on all nodes
  file { "/home/${user}":
    ensure => directory,
  }

  user { $user:
    ensure     => present,
    home       => "/home/${user}",
    managehome => false,
  }

  group { $user:
    ensure => present,
  }

  $packages = [
    'bind-utils',
    'tree',
  ]

  package { $packages:
    ensure => installed
  }
  
  include ntp
  
  file { "/home/${user}/myfile":
    ensure => file,
    notify => Exec['mycommand'],
  }
  
  exec { 'mycommand':
    command => 'echo hello world',
    provider => 'shell',
    refreshonly => true,
  }
}
