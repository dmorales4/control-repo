class profile::wordpress (

){
 include mysql::server
 include wordpress
 include apache
 
 apache::vhost { $facts['fqdn']:
    port    => '8080',
    docroot => '/opt/wordpress',

}

include apache::mod::php

package { 'php-mysqlnd':
    ensure  => installed,
    notify => Service['httpd'],
    
}

}